const getSum = (str1, str2) => {
  // add your implementation below
  const temp = previousChecks(str1,str2);
  if(!temp){
    return temp;
  }

  let startDigit1=str1.length-1;
  let startDigit2 = str2.length-1;
  let remainder = 0;
  let output = [];

  while(startDigit1>-1 || startDigit2>-1 || remainder >0){
    let current = (startDigit1>-1?Number(str1[startDigit1]):0) + (startDigit2>-1?Number(str2[startDigit2]):0)+remainder;
    remainder = 0;
    if(current>9){
      remainder = 1;
      output.push(current-10);
    }
    else{
      output.push(current);
    }
    startDigit1--;
    startDigit2--;
  }

  output = output.reverse();
  return output.join("");
};

const previousChecks = (str1, str2) => {
  if(typeof str1 != "string" || typeof str2 != "string"){
    return false;
  }
  if((str1 != "" && str1.replace(/\D/g,"")=="")||(str2 != "" && str2.replace(/\D/g,"")=="")){
    return false;
  }
  if(str1=="" && str2 !=""){
    return str2;
  }
  if(str1!="" && str2 == ""){
    return str1;
  }
  return true;
}

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  // add your implementation below
  let posts = 0;
  let comments = 0;

  for(let post of listOfPosts){
    if(post.author == authorName){
      posts++;
    }
    if(post.comments != undefined && post.comments.length>0){
      for(let comment of post.comments){
        if(comment.author == authorName){
          comments++;
        }
      }
    }
  }

  return `Post:${posts},comments:${comments}`;
};

const tickets=(people)=> {
  // add your implementation below
  let ticketOffice = {
    "25": 0,
    "50": 0,
    "100": 0
  };

  let i=0;
  while(i<people.length){
    switch(people[i]){
      case 25:
        ticketOffice["25"]++;
        break;
      case 50:
        if(ticketOffice["25"]==0){
          return "NO";
        }
        ticketOffice["25"]--;
        ticketOffice["50"]++;
        break;
      case 100:
        if(ticketOffice["50"]>0 && ticketOffice["25"]>0){
          ticketOffice["100"]++;
          ticketOffice["25"]--;
          ticketOffice["50"]--;
        }
        else if(ticketOffice["25"]>2){
          ticketOffice["100"]++;
          ticketOffice["25"]-=2;
        }
        else{
          return "NO";
        }
        break;
    }
    i++;
  }

  return "YES";
};

module.exports = {getSum, getQuantityPostsByAuthor, tickets};
